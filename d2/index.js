console.log("Hello World");


let cities = ["Tokyo","Manila","Seoul","Jakarta","Sim"];

console.log(cities);

console.log(cities.length);

console.log(cities.length-1);

console.log(cities[cities.length-1]);

let blankArr = [];

console.log(blankArr);

console.log(blankArr.length);

console.log(blankArr[blankArr.length]);

console.log(cities);


//decrement operator can be used to delete the last element of the array
cities.length--;

console.log(cities);

let lakersLegends = ["Kobe", "Shaq","Magic","Kareem","LeBron"];

console.log(lakersLegends);

lakersLegends.length++;
console.log(lakersLegends)


lakersLegends[5] = "Pau Gasol"

// lakersLegends.splice(5,1,"Pau Gasol");
console.log(lakersLegends)

//use the .length property to directly add elements at the end of the array
lakersLegends[lakersLegends.length] = "Anthony Davis";
console.log(lakersLegends);

let numArray = [5,12,30,46,40];

//looping through an array
for(let index = 0; index < numArray.length; index++){
	if(numArray[index] % 5 === 0){
		console.log(numArray[index] + " is divisible by 5");
	}else{
		console.log(numArray[index] + " is not divisible by 5");
	}
}

for(let index = 0; index < numArray.length; index++){
		console.log(numArray[index]);
}

for(let index = numArray.length-1; index >= 0; index--){
		console.log(numArray[index]);
}

//multi-dimensional arrays

let chessBoard = [
["a1","b1","c1","d1","e1"],
["a2","b2","c2","d2","e2"],
["a3","b3","c3","d3","e3"],
["a4","b4","c4","d4","e4"],
["a5","b5","c5","d5","e5"]
];

console.log(chessBoard);
//accessing a multi-dimensional array
console.log(chessBoard[1][4]);

console.log("Pawn moves to " + chessBoard[2][3]);